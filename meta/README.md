# Voting Cards
The cards are colour&letter-coded. The following colour-blindness-friendly colours are used:

| letter | H,S,V        | #RGB     | R,G,B        |
| ------ | ------------ | -------- | ------------ |
| A      | `56,72,94`   | `f0e442` | `240,228,66` |
| B      | `202,100,70` | `0072b2` | `0,114,178`  |
| C      | `41.100.90`  | `e69f00` | `230,159,0`  |
| D      | `164,100,62` | `009e73` | `0,158,115`  |
