# Short Inkscape Overview

Open the file `inkscape_overview_presentation.svg` directly in Inkscape.

License: "CC-BY-NC 4.0 minzma.de https://creativecommons.org/licenses/by-nc/4.0/"

https://inkscape.org

