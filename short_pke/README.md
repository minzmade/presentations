# Presentation

PKE - general explaination about encrypting things with two keys

Drag "`pke.svg`" into your browser and use left and right to navigate though the slides. Hit `i` to view index.

Made with JessyInk in Inkscape.

# License

all Data in this folder [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-nc/4.0/) Benjamin Winter
