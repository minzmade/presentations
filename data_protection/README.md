---
title: "Data Protection"
author: "Benjamin Winter"
date: "September 2022"
license: "CC-BY-NC 4.0 minzma.de https://creativecommons.org/licenses/by-nc/4.0/"

urlcolor: blue
fontsize: 10pt
theme: Frankfurt
colortheme: seahorse
aspectratio: 169
---

## What is Data Protection?
<!--

render with
    pandoc --pdf-engine=xelatex -t beamer -f markdown+emoji -V mainfont="DejaVu Sans" --slide-level 2 --number-section --toc README.md -o data_protection.pdf

-->

:confused: "Data Protection" \tiny "Datenschutz" \normalsize

. . .

:relieved: must protect *data*? \tiny schützt *Daten*? \normalsize

. . .

:open_mouth: No sorry.

. . .

\huge :blush: It's about protecting *people*!

. . .

\normalsize It's a bit like "Fire Protection" \tiny("Brandschutz")\normalsize - not the fire is protected, but the people.  

---

\center
\huge :sunglasses:  
"Data Protection" is about protecting people from others doing unauthorized things with their data.

. . .

\normalsize Yep, it's very bad naming.


# About this Presentation
## Meta
- content
    - simple basic introduction
    - focus on understanding the why
    - not the legal perspective (as I'm an IT guy.)
- target group
    - everybody working with data and may work with personal data
- this presentation
    - is licensed under CreativeCommons BY-NC 4.0 minzma.de \scriptsize <https://creativecommons.org/licenses/by-nc/4.0/> \normalsize
    - is published on Codeberg \scriptsize <https://minzmade.codeberg.page> \normalsize
- find me on Mastodon \scriptsize <https://chaos.social/@minzmade> \normalsize

## Who am I and why do I care?

- software development, server administration, process planning
- worked clinic and study context, proband and patient data
- focussed on doing things right
    - encryption, backup, monitoring, handling data
- GDPR brought a EU legal basis for validating procedures easily if you just switch the perspective (we'll try this later)

. . .

- it just makes sense

# Data Protection
## Laws Laws Laws…
- when deciding on data protection processes, you need a person who is well versed in the legal
- we have GDPR - "General Data Protection Regulation" \tiny
*DSGVO - "Datenschutzgrundverordnung"* \normalsize
- we have German law
- we also have law from the German federal state we are in
- this can get very complex and partly very illogical legal stuff

. . .

- so don't try this alone

. . .

- luckily we are concentrating on the basis, the fundamental logic behind data protection here!

## Whats the basis of data protection?

\center
What do you think?

\tiny
try to fit it into two words

---

\center
\huge :sunglasses:  
\large
The Basis of "Data Protection" is

\huge
Transparency & Communication!

## Background
- privacy is a human right!

. . .

- data protection is for the people!

. . .

- *you* got rights here

## Rights
- *you* have the right to know, what happens with your data before *someone* uses it
- *you* have the right to NOT give your consent or to REVOKE it
- *you* have the right to request of all the data *someone* has about you
- *you* have the right to be forgotten
- *you* got many more rights, read it up in the GDPR, Chapter 3, Art. 12 to 21 "Rights of the data subject" \tiny ( *yes, it's human readable*) \normalsize

\tiny
*you* is a private person; *someone* is a data processor

# Who are the players in data protection?
## Either you use or you are used
Basically two:

- data subject \tiny "Betroffener" \normalsize
    - i.e. a private person with valuable data
- controller \tiny "Verantwortlicher" \normalsize and data processors \tiny "Auftragsdatenverarbeiter" \normalsize
    - a controller defined by GDPR is every non-private party processing data of people of the EU
    - i.e. a company processing and mining data for money

## Controller and Data Processor
- a *data processor* \tiny "Auftragsdatenverarbeiter" \normalsize acts on behalf of a *controller* \tiny "Verantwortlicher" \normalsize and processes data according to the *controllers* instructions
- the data *controller* defines what data is processed and why
- they must have a special contract: "Data Processing Agreement" - DPA \tiny "Auftragsverarbeitungsvertrag" - AVV \normalsize

---

\center
![](images/players_uml.svg){}

## Interactive Part: Who are you?

\center
Please have your voting card ready…

![](images/voting_card_thump.svg){width=30%}


---

![](images/out/interactive_players_Page1.svg){}

---

![](images/out/interactive_players_Page2.svg){}

---

![](images/out/interactive_players_Page3.svg){}

---

![](images/out/interactive_players_Page4.svg){}

---

![](images/out/interactive_players_Page5.svg){}

---

![](images/out/interactive_players_Page6.svg){}

---

![](images/out/interactive_players_Page7.svg){}

---

![](images/out/interactive_players_Page8.svg){}

---

![](images/out/interactive_players_Page9.svg){}

---

![](images/out/interactive_players_Page10.svg){}

---

![](images/out/interactive_players_Page11.svg){}

---

![](images/out/interactive_players_Page12.svg){}

---

![](images/out/interactive_players_Page13.svg){}


## Most important tasks for controllers
Learn from software development:

. . .

1. think about your processes first
1. create a documentation ("Records of Processing Activities" \tiny "Verzeichnis von Verarbeitungstätigkeiten, VVT" \normalsize , Art. 30 GDPR )
1. check with legal department and privacy officer
1. THEN you can start

---

- make your processes transparent to the subject (Art. 5 GDPR)
- inform the subject about every data usage (Art. 5 GDPR)
- get consents where no other lawfulness of processing applies (Art. 6 GDPR)
- ! private data has a life cycle and must die
    - you must to delete it (at the latest, when the purpose is omitted)
    - you do not want to do this manually
    - do not use E-Mail in your personal-data-processes! save personal data in a centralised secured system only

# Personal Data
## Whats that?
\center
What is personal data?

---

\center
\huge :sunglasses:  
\large Personal Data is

\huge data identifying a single person,\
directly or indirectly

\normalsize
*indirectly* means you need another data source to identify the person

## Pseudonymised data and Anonymised data?

- pseudonymised data
    - data that can be mapped to single persons by a reference
    - IS affected by the GDPR, IS personal data
    - i.e. a full medical record without name etc.; a MRI sequence

. . .

- anonymised data
    - it's not possible to find single persons with this data
    - is NOT affected by the GDPR, is NOT personal data
    - i.e. aggregated data; fuzzyfied data; different IDs on different measurements,…

## Special Categories?

There is also a regulation very sensitive data called "special categories" \tiny "besonderen Kategorien" \normalsize that must be particularly well protected.

The penalties here are significantly higher!

*Special Categories* are

- genetic, biometric and health data
- personal data revealing racial and ethnic origin
- political opinions
- religious or ideological convictions
- trade union membership


## Interactive Part: What is this?

\center
Please have your voting card ready…

![](images/voting_card_thump.svg){width=30%}

---

![](images/out/interactive_personal_Page1.svg){}

---

![](images/out/interactive_personal_Page2.svg){}

---

![](images/out/interactive_personal_Page3.svg){}

---

![](images/out/interactive_personal_Page4.svg){}

---

![](images/out/interactive_personal_Page5.svg){}

---

![](images/out/interactive_personal_Page6.svg){}

---

![](images/out/interactive_personal_Page7.svg){}

---

![](images/out/interactive_personal_Page8.svg){}

---

![](images/out/interactive_personal_Page9.svg){}

---

![](images/out/interactive_personal_Page10.svg){}

---

![](images/out/interactive_personal_Page11.svg){}

---

![](images/out/interactive_personal_Page12.svg){}

---

![](images/out/interactive_personal_Page13.svg){}

---

![](images/out/interactive_personal_Page14.svg){}

---

![](images/out/interactive_personal_Page15.svg){}

---

![](images/out/interactive_personal_Page16.svg){}

---

![](images/out/interactive_personal_Page17.svg){}

# Conclusion and Tipps

## If you want to handle personal data, change the Perspective

- Try to view your processes from the subjects view!
- Maybe you are not a big data-mining-company but you also could harm people by misusing their data.
- As an employee of a company you act on behalf of the company.
- Do not decide things alone or with time-pressure.
- Data Protection gives you the chance to make it better (at least prevent scandals).

## Talk to people

- Talk with your team about your processes.
- When it comes to *external Systems used* or data of *external persons used* you need a "Record of Processing Activities" \tiny "Verzeichnis von Verarbeitungstätigkeiten, VVT" \normalsize .
- It's not on you to decide and to create all the documents.
- It's on you to talk to people.

## E-Mails

E-Mails are bad for personal data. Why?

- E-Mails and attachments…
    - are stored in your outbox
    - are stored in the recipients inbox
    - are processed in plain-text on every server (sometimes also every router) between you and the receiver
    - are stored in Cache-Files and other 'artefacts' of your E-Mail-client and the receivers client
- E-Mails are an every-day-thing
    - Users errors will occur (ups, sent to the wrong Petra)
- deleting cannot be done automatically
- deleting WILL NOT BE DONE manually

---

Use a central secured and encrypted system **provided by your company** to store personal data.

i.e.

- a Nextcloud, an OwnCloud
- a dedicated file share with restricted access

## Analogue vs. Digital
- in analogue everything is simpler
- digital data is very volatile (security, safety, human-factor)
- digital always has a rat tail
    - you need somebody, who understands the data processing in the software/service used
    - i.e. your Desktop-Windows-Start-Menu sends every little key-press to Microsoft when you only want to type the name of a program to start (they claim to enhance your user experience)
    - Never store personal data on unencrypted devices!

## Dependencies

Data Protection is highly related to

- FAIR
- Data Quality
- Documentation
- External presentation and Reputation in general

## Data Protection and US Companies

- All companies from the United States of America operate under the "Patriot Act" and must provide all data to the government immediately upon request by a secret court.
- The US-Law does not comply with the GDPR ("Safe Harbour" and "Privacy Shield" violate the law)
- It does not matter, where the servers are.
- All US companies.
\tiny
*Microsoft, Google+Youtube+Chrome, Facebook+WhatsApp+Instagram+Linkedin, Twitter, Dropbox, Zoom, Apple, Amazon, Tesla,…*
\normalsize
- Don't use them for processing personal data!

---

btw…

- Try to understand what you are doing when you use US companies *privately*.
- It's not only your data, but also your family, friends,…
- If an AI decides that your content violates the rules, your account will be deleted.
- Eventually a criminal charges will be filed against you.
- There is no contact person and no way for you to reverse this.

## Data Protection as Blocker?

Data Protection is often used as a scapegoat \tiny Sündenbock \normalsize . In fact it's easy to blame.

. . .

- Tracking of Covid19 infections is not possible - because of Data Protection.
- "We let ourselves be slowed down for a long time in the development of the app by supposed data protection concerns instead of taking advantage of the opportunities for health protection" \tiny about "Corona Warn App" \normalsize
- "The Bavarian school system had developed dynamically during the installation of the software, and data protection had then overturned a few things."
- Emergency Broadcast is not possible - because of Data Protection.
- Speed Limit is not possible - because of Data Protection. \tiny ok, this is a joke \normalsize

. . .

bullshit!

Mostly the real problem: digital incompetence or politically motivated data greed 

---

\center
If you feel, data protection is in your way, slowing you down…

. . .

change the perspective!

As we are talking about *other peoples human rights*.

---

\center
Data Protection is not a Blocker.

It's just your (companies) planning - it's bad.

. . .

You are controller or data processor.

GDPR is protecting people from you.

---

\center
Think first.

. . .

Talk.

. . .

Don't hide your processes.

. . .

Data Protection protects people's freedom and how they make use of it.

---

\center
\huge :sunglasses:  
\large
The Basis of "Data Protection" is

\huge
Transparency & Communication!

\tiny
*internally and externally*
\normalsize

\vspace{30pt}

\hrule

Thank you!

<https://minzma.de>
