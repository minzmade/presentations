# Presentations

Please find my presentation files in the sub folders of this repository.

[Find an end-user-freindly version on my Codeberg-page.](https://minzmade.codeberg.page/).

## Meta-Folder
### Voting Cards
- simple 4-state-voting-cards for small groups
- 10×10cm
- colour-blindness-friendly colours
- letters A-D
- marked back-side

## License

The license for each presentation is usually mentioned in the LICENSE-file and on on of the first slides or the last one.
Mostly this will be a [CreativeCommons-license](https://creativecommons.org/).

If no license is present, I have all copyrights and you must ask before using anything from it (except for personal use) - or I forgot to set the license, which is much more likely.

You can find me on [Mastodon](https://chaos.social/@minzmade).
