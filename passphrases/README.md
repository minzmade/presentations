---
title: "Passwords & Passphrases - how to handle Credentials?"
author: "Benjamin Winter"
date: "October 2022"
license: "CC-BY-NC 4.0 minzma.de https://creativecommons.org/licenses/by-nc/4.0/"

urlcolor: blue
fontsize: 10pt
theme: Frankfurt
colortheme: seahorse
aspectratio: 169
---

# About this Presentation

<!--

render with
    pandoc --pdf-engine=xelatex -t beamer -f markdown+emoji -V mainfont="DejaVu Sans" --slide-level 2 --number-section --toc README.md -o passphrases.pdf

-->

- content
    - simple basic information
    - focus on understanding the why and how
- target group
    - everybody working with digital systems
- this presentation
    - is licensed under CreativeCommons BY-NC 4.0 minzma.de \scriptsize <https://creativecommons.org/licenses/by-nc/4.0/> \normalsize
    - is published on Codeberg \scriptsize <https://minzmade.codeberg.page> \normalsize
- find me on Mastodon \scriptsize <https://chaos.social/@minzmade> \normalsize

## Why is this important?

- fundamental basic of working with digital systems
- your behaviour affects other people
- if a system gets lost or stolen, not only the data on the system but all data of the network can be affected

# Credentials?!

## What are Credentials?

- data needed to log in somewhere
- i.e. URL + username + password

## Why do we need Credentials?

- a way of providing the information, if a subject has the right to access something by using a secret
- examples for the secret part:
    - Passwords and Passphrases (*== looong passwords*)
    - private Keys (i.e. SSH), Certificates (i.e. S-MIME)
    - PIN, PUK, One-Time-Token, Dongle…
        - don't write your PIN on your credit card…
- we will only talk about Passwords/Passphrases here

## Credentials for Security

- other persons, colleagues, companies *must not* access
    - our private files
    - our companies files
    - our keys to other systems
- automatic malware
    - if you system gets infected, the malware will quietly search for information about you, your network and other useful things
    - never have your Credentials unencrypted on any system

## How to have good Credentials?

- the Credentials are the key to our valuable data, they must be
    - **in-accessible** for *anybody* and *any program* - but *you*
        * means: *do never store Credentials in plain digital documents like Text, Word, Notes, E-Mails or "the Cloud"*! Credentials must be encrypted.
    - **un-guessable** for anybody and *any program*
        * means: the Pass* must reach a certain security level
    - **unique** for every singe of our accounts
        * means: do never re-use a password or passphrase

---

\large

\flushleft
:confused: Puh, that sounds pretty complicated!

. . .

\flushright
Yep. Thats why this topic has a bad reputation :neutral_face:

. . .

\flushleft
:disappointed_relieved: So, what next?

. . .

\flushright
I'll show you a trick to manage this by only remembering three passphrases :relieved:

. . .

\flushleft
:open_mouth: What? That sounds easy!

. . .

\flushright
it actually is… :blush:

. . .

\flushleft

:smirk: let's see

\normalsize

## What next?

It can help to understand, WHY you want to do certain things, so the following slides will show

- Good Passwords - a short excursion
    - How are passwords cracked?
    - What is a good password/-phrase and why?
- How to Organise? - Use a Password Manager
    - How to manage all these Credentials and why?
- Summary/Checklists
    - A Check-List-Like-Summary of what (not) to do.

# Good Passwords - a short excursion

## How do bad guys crack our Passwords?

- Phishing & Social Engineering
    - easiest way to get into a building is to have two guys with a ladder
    - attacking the *human factor* is much simpler then technical magic
    - works via
        - E-Mail \
          `This is an urgent request by your IT-Admin, you must change your password here`
        - Telephone \
          `Can you please send me my data again? *crying baby in the background*`
        - in person \
          `Oh I accidentally took this USB stick with me`
    - How to prevent?
        - media competence
        - if something is *URGENT* or *frightening*, take your time and think; check the data you have (i.e. E-Mail-Address)

---

- Malware
    - Spyware, Office-Macros; often together with Phishing
    - How to prevent?
        - don't accept Office-Documents via E-Mail
        - don't open suspicious attachments
        - check senders E-Mail-Address (not the name)
        - check link behind text before clicking it (the "text" can also look like a link)

---

- Password-Lists
    - `try "123456"; try "qwerty123"; try "password"; try "1q2w3e"` - BINGO
        - humans are not creative and often use the same passwords; you can look them up on long lists
    - How to prevent?
        - don't use bad passwords

---

- Brute Force
    - `try "a"; try "b"; try "c"; try "d";… ; try "mySec42!"` - BINGO
    - 10 *thousand* to about 10 *billion* passwords per second (depending on hardware, used hash-algorithm)
    - works offline against hashes, i.e. *John the ripper*

### What's a Hash?
A hash is a one-way encryption of any digital data. It can be used *like a fingerprint* - you can validate the data against the hash. The gimmick is, *you cannot extract the data from a hash*.  
A hash always has the same length.  
Providers are legally required to (also) hash passwords.

```
    sha1sum bla        # 047595d0fae972fbed0c51b4a41c7a349e0c47bb
    sha1sum blo        # f4c993f5bcbdfb82dd2c41012482988d4c154f99
    sha1sum password   # c8fed00eb2e87f1cee8e90ebbe870c190ac3848c
    sha1sum a_film.mp4 # fec37e133997fc35cd536a99ccf564e7b85f3662
```

---

- Rainbow-Tables (should be dead)
    - `look up hashes in a very big database`
    - cracks password in no time but since MD5 is not used anymore and password-hashes are salted nowadays this method is outdated
    - works offline against hashes

## So, what is a good password?

- friends, family and everybody else must not be able to guess it
    - don't use something that is connected to you
- unique
    - if company A has a security leak, you do not want this password to work with company B
- random
    - random characters or words
- long
    - length beats complexity by far!

## Entropy explained (read it later)

\center

![CC-BY-NC 2.5 https://xkcd.com/936/ from 2011](images/password_strength.png){width=55%}

## How do I find a good Passphrase?

- random + easy to remember + hard to crack
    - blindly pick a page of a book and point to a word → repeat
    - OR use Wikipedia.org → Random Article → pick a word → repeat
    - the goal is to have a *good to remember but random sentence* with *more then four random words*, Examples:
        - "The Concert provided by the Swedish Court was great."
        - "I hate the great part control event."
        - "Mein gelber Elefant hat Opa im Kopf."
    - long and easy to remember passwords do not need more time to type then shorter complex ones on the same security level!

## Other methods?

- let the computer generate your password
    - we will come to this later!
    - combines complex passwords and very long passwords
    - not to be remembered by a human

---

There are many other methods, mostly creating complex passwords that are hard to remember, i.e.

Use first letter of every word of a sentence, maybe symbols

- i.e. `Mch4l&lcn.` *My cat has four legs and likes cat nip.*

Use a basic password and add specifiers for the login

- i.e. Basis: `Mch4l&lcn.`;
    - Password for OSM: `Mch4l&lcn.osm.org`
    - Password for Inkscape: `Mch4l&lcn.inkscape.org`
- in this method a part of the password is reused, the security also relies on your trust in the provider to hash and salt your password correctly

### Complex Passwords

*Complex Passwords* are created of more symbols then normally (i.e. lowercase+uppercase+numbers+symbols)  
Make sure, your password has at least 10 characters.

## Interactive Part: Good or Bad

Please get used to your voting system.

---

\center
Hans-Peter, 42a, works since 2006 at CryptCorp, his OS login is

\Large
\center
`hanspeter2006`

. . .

\center
![](../meta/images/head_bad.svg){width=30%}

\normalsize
**AWFULLY BAD**

- very easy guessable

---

\center
Hans-Peter, 42a, works since 2006 at CryptCorp, his OS login is

\Large
\center
`H4n5p3t3r2006`

. . .

\center
![](../meta/images/head_bad.svg){width=30%}

\normalsize
**AWFULLY BAD**

\flushleft
- using something like 'leet speak' does not enhance the password strength

---

\center
Hans-Peter, 42a, works since 2006 at CryptCorp, his OS login is

\Large
\center
`hans42@CryptCorp!1`

. . .

\center
![](../meta/images/head_bad.svg){width=30%}

\normalsize
**BAD**

- very common password-scheme \small (*name@company, may add some numbers, symbols at the end*)

---

\center
Hans-Peter, 42a, works since 2006 at CryptCorp, his OS login is

\Large
\center
`ko6±^dC¼q-§ÈñìÜÖ(ÛJIµ®Ë]É.I§%<Kç¯ócD4gI£nrüÄôÆmÈ`

. . .

\center
![](../meta/images/head_bad.svg){width=30%}

\normalsize
**BAD**

\flushleft
- it' a very good long and complex generated passphrase
- as it is *for the OS login* Hans-Peter will have no possible way to use a password manager here - so how does he log in?
- there is no way, he can use this in a secure way

---

\center
Hans-Peter, 42a, works since 2006 at CryptCorp, his OS login is

\Large
\center
`I hate my Job!`

. . .

\center
![](../meta/images/head_bad.svg){width=30%}

\normalsize
**AWFULLY BAD**

- common, short, not random

---

\center
Hans-Peter, 42a, works since 2006 at CryptCorp, his OS login is

\Large
\center
`]*NgelwPm}y0F_`

. . .

\center
![](../meta/images/head_middle.svg){width=30%}

\normalsize
**could be optimised**

\flushleft
- strong enough, it's a good complex password
- no fun typing this manually (OS login)
- no fun remembering this

---

\center
Hans-Peter, 42a, works since 2006 at CryptCorp, his OS login is

\Large
\center
`The glimpse of amazingly squeamish endeared earthlings.`

. . .

\center
![](../meta/images/head_good.svg){width=30%}

\normalsize
**EXCELLENT**

\flushleft
- long, random, rememberable

---

\center
Hans-Peter, 42a, works since 2006 at CryptCorp, his OS login is

\Large
\center
`correct horse battery staple`

. . .

\center
![](../meta/images/head_bad.svg){width=30%}

\normalsize
**BAD**

\flushleft
- as this passphrase is the example of the famous xkcd#936, it is on every password list and therefore not random anymore

---

\center
A Hotel provides WiFi for their guests, the Password is

\Large
\center
`AllYouNeedIsLove`

. . .

\center
![](../meta/images/head_good.svg){width=30%}

\normalsize
**OK!**

\flushleft
- as many people will get this password there is no need here to use something very secure

---

\center
The PIN of Alice' bank card is

\Large
\center
`5731`

. . .

\center
![](../meta/images/head_good.svg){width=30%}

\normalsize
**OK!**

\flushleft
- even, if this is really short and easy to brute-force it's secure, as you have only three guesses

---

\center
Charlies Passphrase for the E-Mail is

\Large
\center
`AllYouNeedIsLove`

. . .

\center
![](../meta/images/head_bad.svg){width=30%}

\normalsize
**BAD**

\flushleft
- common password
- not random
- if it's related to Charlie (favourite song) it's even worse

---

\center
Adas Passphrase for the OS Account is

\Large
\center
`q2w3e4/u8i9o0#`

. . .

\center
![](../meta/images/head_bad.svg){width=30%}

\normalsize
**VERY BAD**

\flushleft
- parts from common password
- not random (look at your keyboard)

---

\center
Rüdigers Passphrase for the Company Account is

\Large
\center
`hjkhjkhjk!bhubhubhu/vbnvbnvbn`

. . .

\center
![](../meta/images/head_bad.svg){width=30%}

\normalsize
**VERY BAD**

\flushleft
- parts from common password
- not random (look at your keyboard)
- repetitions will not enhance your password

---

\center
Eves Passphrase for the Company Account is

\Large
\center
`Two promptly Horses are uselessly to undergo.`

. . .

\center
![](../meta/images/head_good.svg){width=30%}

\normalsize
**EXCELLENT**

\flushleft
- long, random, rememberable

## Automatic Tool to check passwords?

There are tools, that try to calculate the strength of passwords, but as it is hard/impossible for a computer to understand content, this is not an easy task.

A good tool will check your input *locally only* against

- length
- commonly known passwords
- repetitions
- listed content (i.e. your companies name)

Please remember, that these tools can help, but cannot think for you.

## Help - I cannot use the password I want to :(
- provider has a maximum length for my password
    - usually there is no technical reason for a max. length
    - use the maximum allowed
    - only use a generated complex password
    - may ask the provider for the technical reason

. . .

- the provider forces me to use lowercase, uppercase, numbers and symbols
    - this is common, try to adapt you passphrase, i.e.
        - "I hate the great part control event." → "I hate the 2 great part control events."
    - or use a generated complex password
    - may create an issue for your provider, link xkcd#936

. . .

- I cannot use spaces/Umlauts/Symbols/…
    - use a generated password avoiding these characters
    - may create an issue for your provider, link xkcd#936

# How to Organise? - Use a Password Manager

## What's a Password Manager?
- a software that stores your login data, keys, certificates,…

. . .

- locally on your profile

. . .

- in a secure encrypted file called *database*

. . .

- protected by a Passphrase

## Live presentation

This is how a used Password Manager looks like.

\center
![](images/Screenshot_2022-10-26_10-38-56.png){width=80%}

---

Create a new database - use a Passphrase of 15++ characters!

\center
![](images/Screenshot_2022-10-26_10-41-07.png){width=60%}

---

Add/Update/Move/Delete folders

\center
![](images/Screenshot_2022-10-26_10-55-53.png){width=80%}

---

Add/Update/Move/Delete entries

\center
![](images/Screenshot_2022-10-26_10-45-58.png){width=80%}

---

Generate Secrets - see the Entropy and Quality

\center
![](images/Screenshot_2022-10-26_10-43-37.png){width=80%}

---

Login somewhere using an entry in your Password Manager - test copying and shortcuts

\center
![](images/Screenshot_2022-10-26_10-46-32.png){width=80%}


## Where to store the Passphrase for the Password Manager?
- there are Passphrases you have to enter manually, i.e.
    - to open your Password Manager
    - to log in on your OS
    - to encrypt your disk
    - to log in on your companies main account
- add them also to your Password Manger but *also write them down on paper and store it in a secure location*

## What else?
Make sure, your database of the Password Manager is in the Backup.

## All Logins saved in one file? Is that secure?
- it highly depends on
    - your password manager
    - your passphrase
    - what else is running on your system (keylogger, spyware, bad clipboard manager, your OS in general…)
- yes, it IS secure for the most common users
- use password managers!

## Whats about my WebBrowser/MailClient?
- your Web Browser/Mail Client will usually offer you to save your login data
    - this is handy and in the right place
    - *you can do this under TWO important requirements*:

## Requirements on saving login data in the WebBrowser/MailClient?
1. you *must use* a "Primary Password"/"Main Password" \footnotesize (formerly known as "Master Password") \normalsize
    - this is a Passphrase, and it's highly critical to have a secure one here
    - this will encrypt all your login data
    - you must enter this Passphrase once when the software starts
    - save the Passphrase in your Password Manager
1. the software must be trusted
    - if you want to use this in your company, talk to the IT-SEC beforehand!

## Other Methods of storing Credentials?

- a physical password book
    - pretty secure, but you will have to enter every password manually every time
    - not usable for work, the risk is too high
- "Keyring"
    - basically a password manager in the background, provided by your OS

## Whats with the Cloud?

- personal use
    - it highly depends on trusting the provider
    - using free software (free, libre and open source), maintained by the community, enhances transparency and therefore security
- Do not use any Cloud to store your work-related Credentials!

# Summary/Checklists

## General Credential-handling 1

- use a password manager! i.e. [KeePassXC.org](https://keepassxc.org)
- secure the password manager storage with a Passphrase (15++ Characters)
    - use a sentence you can remember easily but nobody would expect, use random words in it, (5 words min)
    - write down your Credentials on a sheet of paper and store it in a secure physical location
- passphrases == long passwords, they are MUCH better then shorter complex passwords
    - passwords with less then 8 characters are NO passwords
    - passwords with less then 10 characters are weak
- for accounts where you have to enter your Credentials manually
    - use self-made passphrases, the longer the better
- for accounts where you enter your Credentials with your password manager
    - use complex generated passphrases of about 23+ characters

## General Credential-handling 2

- You can store passwords in trustworthy local applications, BUT only if they are encrypted with a *Primary Key*
    - the *Primary Key*
        - is a Passphrase, that you store in your password manager
        - acts as the Key to the stored login data inside this local application (just like in your password manager)
- do never share your passwords with your friends (sharing is NOT a proof of trust)
- do never share your passwords with your admin (they never need your password)
- never re-use passwords

## How to create new passwords/passphrases?

1. add new Item to your Password Manager
1. generate a strong passphrase in your Password Manager (23+ Characters)
1. save it and only use it from your Password Manager

## How to backup all my Credentials?

- make sure the database (simple file) of your Password Manager is in the backup
- OR individually push it to your private git-repository
- OR make a manual backup on an USB-Stick

## What do to, if I accidentally saved a Credential un-encrypted?

- change it!

## How to delete Credentials/Data securely?

- on paper
    - use the data garbage can
    - or shred it - not in strips, but pieces
    - or burn it
- digitally
    - you cannot delete it securely - change your Credentials
    - deleting somthing from an HDD is possible, deleting from an SSD would be very hard
    - deleting something from "the internet" is like fishing pee out of the pool

---

\center
\huge
Thank you :)

\tiny
My Name is my Password.

## This is a fancy Test-Slide!

Holla, this is text, can you read it?

- bullet
- points \tiny this is challenging, right? \normalsize
- *also in italic* \tiny
  *uuhhh, are you serious??*
  \normalsize

\center
\huge
ok, done
\normalsize
