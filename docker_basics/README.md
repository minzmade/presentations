---
title: "Docker-Overview-Presentation-Experience-Sharing"
author: "Benjamin Winter"
date: "June 2022"
license: "CC-BY-NC 4.0 minzma.de https://creativecommons.org/licenses/by-nc/4.0/"

urlcolor: blue
fontsize: 10pt
theme: Frankfurt
colortheme: seahorse
aspectratio: 169
---

## About this Presentation
<!--
render with
    pandoc --pdf-engine=xelatex -t beamer -f markdown+emoji -V mainfont="DejaVu Serif" --slide-level 2 --number-section --toc README.md -o docker_basics.pdf
-->

- content
    - simple introduction
    - focus on understanding, best practice, not coding
- target group
    - everybody interested in Science, Coding, FAIR, Tools, …
- no words regarding security
    - learn something about rootless-mode before using Docker in public environment
- this presentation
    - is licensed under [CreativeCommons BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)
    - is published on Codeberg \scriptsize <https://minzmade.codeberg.page> \normalsize
- find me on Mastodon \scriptsize <https://chaos.social/@minzmade> \normalsize

# What is Docker
## Virtualisation
- run processes, scripts, operating systems or networks virtually with Docker
- like Virtual Machine but different
    - Docker shares resources with the Linux environment
    - if you want to work with malware, use a VM, better a dedicated Computer

|                    | VM   | Docker   |
| ------------------ | ---- | -------- |
| size               | big  | small    |
| startup time       | slow | fast     |
| stateful container | yes  | no, but… |

## Science- and Development-friendly
- documentation of what was done is included directly (`Dockerfile`)
- helps people to produce understandable and reproducible code

. . .

- did you ever tried to reproduce results from a paper with their script?
    - "i used the numpy, but forget the version and another library but cannot remember the name, the one for the plots, you know? you have to install this first, but before you need postgresql configured for local-user with superuser rights, i'm sure you know, what i mean…"
    - versions used missing
    - info about environment missing
    - license missing
    - code not in version control
    - no documentation
    - …

## Can be used for
- easy run and use software without installing it on the local machine
    - i.e. run OpenRefine (2D-Date cleaning tool)
        - `docker run --rm -p 3333:3333 vimagick/openrefine`
- try out things, install, configure, break, restart, learn
- build the environment for your software/script to run out of the box




# Basic Concept of Docker
## About
- Docker (Client, Engine) is free software (FLOSS)
    - "Docker Desktop" is licensed differently but not needed
- Docker runs on Linux
- shares system resources with Linux
- can also run on Windows, uses the Linux Subsystem running in Windows (WSL)

## Overview
![the plan](images/dockerfile_image_container.svg){height=70%}

## Dockerfile
- a real text file
- *is* the documentation
- define, whats in the `Image`

## Docker Image
- made from a `Dockerfile`
- can be run by Docker

## Docker Container
- a running system in docker, started from an `Image`
- has a state as long as it runs - changes
- files or ports from the host system can be embedded seamlessly
- runs until process is finished

## Docker Volume
- a way for storing data, managed by Docker
- can be used to avoid problems with user/group-rights defied by the file system

## Docker Network
- connections made by docker between multiple running `Container`s or `Volume`s
- use `docker-compose` to manage `Container`s with a YAML-File (also text based)

## Docker-Commands
- commands you call on the host system running the Docker-Engine\ (`docker-cli`)
- manage `Image`s, `Container`s, `Network`s etc.
- BUILD - to create an `Image` from a `Dockerfile`
- RUN - here goes everything that's important for
    - connecting the host to the `Container`
        - forward ports
        - "bind mount" files/folders from host to `Container`
        - other options for running the `Container`
    - connecting `Container`s, `Network`s and `Volume`s,…

## Overview Basics
:::::::::::::: {.columns}
::: {.column}

<!--
evil bad way of centring the content here
align=center does not work
if you know the right way, please let me know!
-->
```py
 
 
 
 
 
```

- `Dockerfile` \tiny{} basic rules in text format \normalsize{}
- `Image` \tiny{} made from a `Dockerfile` \normalsize{}
- `Container` \tiny{} running `Image` \normalsize{}
- `Volume` \tiny{} a Docker-internal storage \normalsize{}
- `Network` \tiny{} connect `Container`s and `Volume`s \normalsize{}

:::
::: {.column}
![the plan](images/dockerfile_image_container.svg){height=70%}
:::
::::::::::::::




# Short Excursus: States
## What is a State?
- a variable

```py
my_var = 42         # my_var is 42

for i in range(5):
	my_var += i     # add something to my_var

print(my_var);      # will be 52
```

## What is a State?
- an object
    - has variables (attributes)
    - is stateful
    - parent object may also has a state

```py
class Person(Human):
    count=0
    def __init__(self,name="NoName",age=-1):
        self.name=name
        self.age=age
    ...
```

## What is a State?
- think of the operating system
    - has a version
    - installed programs and libraries have a version
    - running multiple processes with states

## States - Conclusion
- states make everything complicated, also in programming
    - you must check states in your code
    - you must create unit-tests to check you code
    - the more possible states the more complicated it is
    - also best practice in programming, i.e. use data-objects instead

. . .

- less states and therefore less dependencies and less uncertainties → better

## Stateless in Docker
- avoid states in `Container`s!
    - Docker `Container`s will have a state while running
    - if they are done with there work, Docker will destroy the `Container`
    - if you need the process gain, run a fresh `Container`

. . .

- benefits
    - by defining the environment in Docker, you can concentrate on your code only
    - it will also work on any other machine
    - don't forget to define the version of `Image`s/software used in your `Dockerfile`

. . .

- do not rely on the state of a `Container`
    - best: always start `Container`s with `--rm` \tiny{} will be deleted after finishing \normalsize{}

. . .

- Docker `Volume`s or the host-file-system or a database are for saving Data, not the `Container`s




# Docker Example
## Basic Commands and Tipps
- use power of the Linux Shell
    - help on possible commands, parameters, usage, meaning,…
        - use the `TAB` key, may press twice
        - use the help, i.e. `docker run --help`
        - use the man-page, i.e. `man docker run`

. . .

- search an existing `Image`: `docker search debian`
    - also find it on DockerHub: <https://hub.docker.com/_/debian>
- build an `Image` from a `Dockerfile` in current folder: `docker build --no-cache -t my_cool_image_name .`
- run an existing `Image`: `docker run --rm -it my_cool_image_name --name my_cool_container_name`
- show running container: `docker ps`
- stop a running container: `docker stop my_cool_container_name` \tiny{} use `kill` to force stop \normalsize{}

## The Plan
:::::::::::::: {.columns}
::: {.column}

<!--
evil bad way of centring the content here
align=center does not work
if you know the right way, please let me know!
-->
```py
 
 
 
 
 
```

- let's create a plain Debian system, then
    - install an Apache web-server
    - configure the Apache with external file
    - show a "static homepage" based on files on the host system
    - forward container-internal port 80 to host-port 8080
- practical lesson

:::
::: {.column}
![the plan](images/example_structure.svg){width=80%}
:::
::::::::::::::




# What we've learned
## Summary
- Docker is used for virtualisation
- a `Dockerfile` defines everything
    - will be build to an `Image`
    - will run as a `Container`
- as Docker brings the documentation for the final `Image` with the `Dockefile`, it's highly reusable
- stateful `Container`s are bad
- practical lesson
- most important to do a clean job
    - stick to clean-code guidelines
    - write things down, so that *others* can understand, whats happening

## Links
- Docker Documentation: <https://docs.docker.com>
- Docker-Hub (find `Image`s): <https://hub.docker.com>
- Wikipedia-Page for Docker: <https://en.wikipedia.org/wiki/Docker_(software)>
- Rootless mode: <https://docs.docker.com/engine/security/rootless/>
- Wait-for-it-Script: <https://github.com/vishnubob/wait-for-it>
- Install Docker on Debian-based Systems: <https://docs.docker.com/install/linux/docker-ce/debian/>

## Cheat-Sheet: More useful Docker Commands
- `docker images` \footnotesize{} *- all about Docker `Image`s* \normalsize{}
- `docker inspect image_or_container_name` \footnotesize{} *- inspect `Container`/`Image`* \normalsize{}
- `docker logs container_name` \footnotesize{} *- show logs of `Container`* \normalsize{}
- `docker run … --user "$(id -u):$(id -g)" …` \footnotesize{} *- use the same User- and Group-ID in the `Container` as you have in host System* \normalsize{}
- `docker exec -u 0 -it mytest bash` \footnotesize{} *- start a root-shell in running `Container` "mytest"* \normalsize{}
- `docker network` \footnotesize{} *- all about docker networks* \normalsize{}
- `docker ps --size` \footnotesize{} *- check disk usage* \normalsize{}
- `docker system prune` \footnotesize{} *- delete all resources (images, containers, volumes, and networks) that are dangling (not associated with a container)* \normalsize{}
- `docker rm $(docker ps -a -f status=exited -q)` \footnotesize{} *- remove all non-running containers* \normalsize{}
