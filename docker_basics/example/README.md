# Info

this folder contains a docker example with a apache webserver running inside a docker container mounting a folder on the host system with the website content

# Cheat Sheet for the presentation

```sh
docker search debian

# docker build --no-cache -t a:b .
docker build --no-cache -t my_image .


docker run --rm -it my_image --name my_container

docker run --rm -p8080:80 --name my_container my_image

# docker run --rm --publish 8080:80 --Volume ${PWD}/website/:/var/www/html/:ro --detach --name my_container my_image
docker run --rm -p 8080:80 -v ${PWD}/website/:/var/www/html/:ro -d --name my_container my_image


docker ps
docker stop my_container
docker kill my_container

docker exec -u 0 -it my_container bash
```
